# rt_lite
rt_lite is a simple Vue app used to present movie scores from Rotten Tomatoes in
a simple, easy-to-skim format.  The front page presents current and upcoming
movies.  The search field allows you to search for a film and see its score.

Search terms are sent to Rotten Tomatoes as-is, and their results are then
formatted and presented in a simple list.  If you think your search isn't
returning what it should be, try searching for the same terms on Rotten
Tomatoes.  If the results are different,
[create an issue](https://github.com/gabeotisbenson/rt_lite/issues/new) and let
me know!

The results are color-coded for easier glancing: red means the movie is fresh,
green means the movie is rotten, and black means the movie either has no score
or has not yet reached a consensus.

The aim of the project was to do simply what it does, and not much else.  The
movie blocks are links to the movies' pages on Rotten Tomatoes, so if you'd like
more info about a film you may click away and read up.

## Contributing
- [Fork](https://github.com/gabeotisbenson/rt_lite/fork) this repository
- Create a new branch and make your changes
- Send a [pull request](https://help.github.com/articles/creating-a-pull-request) to upstream (gabeotisbenson/rt_lite)
