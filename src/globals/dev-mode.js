/* eslint no-process-env: 0 */
export default process.env.NODE_ENV === 'development';
