import devMode from '~/globals/dev-mode';
export default devMode ? 'http://localhost:8081/' : 'https://cors.gabe.xyz/';
